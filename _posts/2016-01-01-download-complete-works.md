
<strong>Books on Tantra Maya by Vishal Eckhart</strong>


One may read online or download a .pdf file


<strong>
A Name to the Nameless  </strong> First work that began in 1996.  A psychological explanation of tantric meditation and philosophy.  <a href="http://data.elmisterio.org/books/a-name-to-the-nameless.pdf">Download "A Name To The Nameless"</a> 


<strong>
Microvita and Tantra Maya  </strong> More free form and experimental than A Name to the Nameless.  An explaination of the concept of Microvita in relation to the Meso-American practice of Tantra Maya. <a href="http://data.elmisterio.org/books/microvita-and-tantra-maya.pdf">Download "Microvita and Tantra Maya"</a>


<strong>
Anahata  </strong>  A chapter from Microvita and Tantra Maya.  An explanation of the yogic Anahata Chakra or Spiritual Heart. 
<a href="http://data.elmisterio.org/books/anahata.pdf">Download "Anahata"</a>



<strong>Tantric Rebellion and Kundalini  </strong>  Subjective introspections on kundalini and spiritual rebellion.  <a href="http://data.elmisterio.org/books/tantric-rebellion-and-kundalini.pdf">Download "Tantric Rebellion and Kundalini"</a>


<strong>Books on Tantra Maya by Quetzal Eckhart</strong>

<strong>
<a href="http://elmisterio.org/a-name-to-the-nameless-complete/">A Name to the Nameless</a> </strong> First work that began in 1996.  A psychological explanation of tantric meditation and philosophy.  


<strong>
<a href="/microvita-and-tantra-maya-complete/">Microvita and Tantra Maya</a>  </strong> More free form and experimental than A Name to the Nameless.  An explaination of the concept of Microvita in relation to the Meso-American practice of Tantra Maya. 

<strong>
<a href="http://elmisterio.org/anahata/">Anahata</a></strong>  A chapter from Microvita and Tantra Maya.  An explanation of the yogic Anahata Chakra or Spiritual Heart. 

<strong> <a href="http://elmisterio.org/tantric-rebellion-and-kundalini/">Tantric Rebellion and Kundalini</a></strong>  Subjective Introspections on kundalini and Spiritual Rebellion.  



<strong>Complete Works of Anandamurti</strong>: <a href="https://cmdr0.blaucloud.de/index.php/s/VZXchJaawWf93SI">Download</a>





<strong>Music</strong>: <a href="http://elmisterio.org/flauta-de-bambu/">Musica Misterio</a>

